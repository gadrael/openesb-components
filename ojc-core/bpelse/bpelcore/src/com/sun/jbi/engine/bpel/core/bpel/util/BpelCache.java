package com.sun.jbi.engine.bpel.core.bpel.util;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public interface BpelCache {
    
    String put(String key, String value);
    
    String get(String key);
    
    String remove(String key);
}
