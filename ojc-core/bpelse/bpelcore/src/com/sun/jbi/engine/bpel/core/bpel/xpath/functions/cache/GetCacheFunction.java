package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.util.BpelCache;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class GetCacheFunction extends AbstractCacheFunction {
    
    public GetCacheFunction(BpelCache bpelCache) {
        super(bpelCache);
    }
    
    public Object invoke(ExpressionContext context, Object[] parameters) {
        return bpelCache.get((String) BPWSFunctions.convertParam(parameters[0]));
    }

}
